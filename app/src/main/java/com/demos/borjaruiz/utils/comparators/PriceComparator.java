package com.demos.borjaruiz.utils.comparators;

import com.demos.borjaruiz.model.Track;

import java.util.Comparator;

public class PriceComparator implements Comparator<Track> {
    @Override
    public int compare(Track o1, Track o2) {
        return Double.compare(o1.getTrackPrice(), o2.getTrackPrice());
    }
}