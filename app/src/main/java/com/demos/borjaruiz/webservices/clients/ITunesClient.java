package com.demos.borjaruiz.webservices.clients;

import com.demos.borjaruiz.webservices.responses.iTunes.TrackResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ITunesClient {
    @GET("/search")
    Call<TrackResponse> getTracks(@Query("term") String searchParams);
}