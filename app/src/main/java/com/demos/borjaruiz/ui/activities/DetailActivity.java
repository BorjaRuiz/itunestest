package com.demos.borjaruiz.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.demos.borjaruiz.R;
import com.demos.borjaruiz.presenters.implementations.TrackDetailsPresenterImpl;
import com.demos.borjaruiz.presenters.interfaces.TrackDetailsPresenter;
import com.demos.borjaruiz.ui.views.TrackDetailActivityView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailActivity extends BaseAppCompatActivity implements TrackDetailActivityView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv1)
    TextView artistName;
    @BindView(R.id.tv2)
    TextView collectionName;
    @BindView(R.id.tv3)
    TextView duration;
    @BindView(R.id.tv4)
    TextView releaseDate;
    @BindView(R.id.iv_image)
    ImageView albumImage;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.play_pause)
    ImageView play_pause_bottom;

    TrackDetailsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        presenter = new TrackDetailsPresenterImpl(this);
    }

    @Override
    public void loadDetails(String name, String artist, String image, String collection, String dur, String release) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(name);//TODO revisar porqué no cambia titulo al cambiar canción
        }
        Picasso.with(this).load(image).into(albumImage);
        artistName.setText(name);
        collectionName.setText(collection);
        duration.setText(dur);
        releaseDate.setText(release);
    }

    @Override
    public void showPlayIcon() {
        play_pause_bottom.setImageResource(R.drawable.ic_play_vector);
    }

    @Override
    public void showPauseIcon() {
        play_pause_bottom.setImageResource(R.drawable.ic_pause_vector);
    }

    @Override
    public void showBuyAlert(String title, final String url) {
        AlertDialog.Builder b = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.placeholder_buy, title))
                .setMessage(getString(R.string.dialog_buy_message))
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Intent i = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(url));
                                startActivity(i);
                            }
                        }
                )
                .setNegativeButton(android.R.string.cancel, null);
        b.show();
    }

    @OnClick({R.id.fab})
    public void buyTrack() {
        presenter.buyClicked();
    }

    //Onclick controles mediaplayer
    @OnClick({R.id.next, R.id.previous, R.id.rewind, R.id.forward, R.id.play_pause})
    public void controlButtonClicked(View v) {
        switch (v.getId()) {
            case R.id.previous:
                presenter.previousTrack();
                break;
            case R.id.next:
                presenter.nextTrack();
                break;
            case R.id.play_pause:
                presenter.playOrPause();
                break;
            default:
                toast(getString(R.string.toast_not_implemented));
                break;
        }
    }


    @Override
    protected void onDestroy() {
        presenter.unRegisterEvents(); //dejamos de escuchar eventos
        super.onDestroy();
    }
}
