package com.demos.borjaruiz.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.demos.borjaruiz.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseAppCompatActivity extends AppCompatActivity {
    private ProgressDialog mProgressDialog;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    //Fuentes custom
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    //Para mostrar dialogs fácilmente
    protected void loading() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            loaded();
        mProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.app_name), getResources().getString(R.string.pd_cargando), true, false);

    }

    protected void loaded() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        loaded();
        super.onDestroy();
    }
}
