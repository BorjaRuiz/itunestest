package com.demos.borjaruiz.ui.views;

import android.os.Bundle;
import android.view.View;

import com.demos.borjaruiz.model.Track;

import java.util.ArrayList;

public interface TrackListActivityView {
    void onNoResults(String text);

    void refreshList(ArrayList<Track> refreshed_tracks);

    void goDetailTrack(View view, Bundle bundle);

    void nothingToSort();

    void showLoading();

    void dismissDialog();
}