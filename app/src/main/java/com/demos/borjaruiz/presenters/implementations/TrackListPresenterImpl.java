package com.demos.borjaruiz.presenters.implementations;

import android.os.Bundle;
import android.view.View;

import com.demos.borjaruiz.R;
import com.demos.borjaruiz.controllers.MediaPlayerController;
import com.demos.borjaruiz.interactors.DataInteractor;
import com.demos.borjaruiz.interactors.ITunesDataInteractorImpl;
import com.demos.borjaruiz.model.EventBus.SongFinishedEvent;
import com.demos.borjaruiz.model.EventBus.SongPlayingEvent;
import com.demos.borjaruiz.model.Track;
import com.demos.borjaruiz.presenters.interfaces.TrackListPresenter;
import com.demos.borjaruiz.ui.views.TrackListActivityView;
import com.demos.borjaruiz.utils.AppConstants;
import com.demos.borjaruiz.utils.callbacks.DataCallback;
import com.demos.borjaruiz.utils.comparators.GenreComparator;
import com.demos.borjaruiz.utils.comparators.PriceComparator;
import com.demos.borjaruiz.utils.comparators.TimeComparator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;

public class TrackListPresenterImpl implements TrackListPresenter {
    private TrackListActivityView view;
    private DataInteractor interactor;

    public TrackListPresenterImpl(TrackListActivityView view) {
        this.view = view;
        interactor = new ITunesDataInteractorImpl();
        //Si quisieramos obtener los datos de otro servicio o de una base de datos local cambiaríamos el interactor.
        registerEvents();
    }

    @SuppressWarnings("unchecked")  //el result siempre será array de Track, venga de dónde venga.
    @Override
    public void findTracks(final String searchText) {
        view.showLoading();
        interactor.getData(searchText, new DataCallback() {
            @Override
            public void onComplete(Object result) {
                view.dismissDialog();
                getMediaPlayer().setTracks((ArrayList<Track>) result);
                updateList();
            }

            @Override
            public void onNoData() {
                getMediaPlayer().setTracks(new ArrayList<Track>());
                view.dismissDialog();
                view.onNoResults(searchText);
            }

            @Override
            public void onError(Error error) {
                view.dismissDialog();
            }
        });
    }

    private void registerEvents() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unRegisterEvents() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onItemClick(View v, int position) {
        Bundle bundle = new Bundle();
        bundle.putParcelable((AppConstants.EXTRA_TRACK), getMediaPlayer().getTracks().get(position));
        setCurrentPlaying(position);
        view.goDetailTrack(v, bundle); //Vamos al detalle de la clicada
    }

    private void setCurrentPlaying(int playing) {
        getMediaPlayer().setCurrentPlaying(playing);
        updateList();
    }

    @Override
    public void orderBy(int orderName) {
        if (getMediaPlayer().getTracks().isEmpty())
            view.nothingToSort();
        else {
            switch (orderName) { //Actualizamos el orden del mediaPlayer
                case R.id.order_price:
                    Collections.sort(getMediaPlayer().getTracks(), new PriceComparator());
                    break;
                case R.id.order_genre:
                    Collections.sort(getMediaPlayer().getTracks(), new GenreComparator());
                    break;
                case R.id.order_time:
                    Collections.sort(getMediaPlayer().getTracks(), new TimeComparator());
                    break;
            }
            updateList();
        }
    }

    @Override
    public Bundle getBundleToSave() {
        Bundle bundle = new Bundle(); //Mostramos las canciones que ya teníamos
        bundle.putSerializable(AppConstants.STATE_ITEMS, getMediaPlayer().getTracks());
        return bundle;
    }

    @Override
    public void onRotateScreen() {
        updateList();
    }

    @Subscribe
    public void onSongPlayingEvent(SongPlayingEvent event) {
        setCurrentPlaying(event.position);
        updateList();
    }

    @Subscribe
    public void onSongFinishedEvent(SongFinishedEvent event) {
        //Podríamos usar este evento para hacer algo al acabar de sonar una canción
    }

    private MediaPlayerController getMediaPlayer() {
        return MediaPlayerController.getInstance();
    }

    private void updateList() {
        view.refreshList(getMediaPlayer().getTracks());
    }
}
