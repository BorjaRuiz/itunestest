package com.demos.borjaruiz.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.view.View;

import com.demos.borjaruiz.R;
import com.demos.borjaruiz.ui.activities.DetailActivity;

public class NavigationController {
    private static NavigationController ourInstance = new NavigationController();

    public static NavigationController getInstance() {
        return ourInstance;
    }

    private NavigationController() {
    }

    public void goTrackDetailActivity(Activity context, View v, Bundle b) {
        Pair<View, String> p1 = Pair.create(v, v.getContext().getString(R.string.transition_details));
        Pair<View, String> p2 = Pair.create(v.findViewById(R.id.iv_image), v.getContext().getString(R.string.transition_image));
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(context, p1, p2);
        ContextCompat.startActivity(context, new Intent(context, DetailActivity.class).putExtras(b), options.toBundle());
    }
}
