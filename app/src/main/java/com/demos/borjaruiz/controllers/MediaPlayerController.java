package com.demos.borjaruiz.controllers;

import android.media.AudioManager;
import android.media.MediaPlayer;

import com.demos.borjaruiz.model.EventBus.SongPlayingEvent;
import com.demos.borjaruiz.model.Track;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;

public class MediaPlayerController implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {

    private static final String TAG = "MEDIAPLAYER_CONTROLLER";
    private static MediaPlayerController INSTANCE = null;
    /*Link donde se contradicen con el "warning" del static context:
    https://developer.android.com/training/volley/requestqueue.html#singleton  */

    private MediaPlayer mediaPlayer;
    private ArrayList<Track> tracks;
    private int currentPlaying;
    private boolean isPaused;
    private final int FIRST_TRACK = 0;

    private MediaPlayerController() {
        tracks = new ArrayList<>();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
    }

    private synchronized static void createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MediaPlayerController();
        }
    }

    public static MediaPlayerController getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

    public void setTracks(ArrayList<Track> tracks) {
        this.tracks = tracks;
    }

    public ArrayList<Track> getTracks(){
        return tracks;
    }

    public void switchState() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            isPaused = true;
        } else {
            isPaused = false;
            mediaPlayer.start();
        }
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void play() {
        playTrack(currentPlaying);
    }

    public void playNextTrack() {
        if (currentPlaying == tracks.size() - 1) //si es la última de la lista reproduzco la primera
            currentPlaying = FIRST_TRACK;
        else
            currentPlaying++;
        play();
    }

    public void playPreviousTrack() {
        if (currentPlaying == FIRST_TRACK) //si es la primera de la lista reproduzco la última
            currentPlaying = tracks.size() - 1;
        else
            currentPlaying--;
        play();
    }

    private void playTrack(int position) {
        currentPlaying = position;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(tracks.get(position).getPreviewUrl());
        } catch (IllegalArgumentException e) {
            //TODO
        } catch (SecurityException e) {
            //TODO
        } catch (IllegalStateException e) {
            //TODO
        } catch (IOException e) {
            //TODO
        }
        mediaPlayer.prepareAsync();
        //Al poner aquí el evento permitimos mostrar info mientras el mediaPlayer se prepara.
        EventBus.getDefault().post(new SongPlayingEvent(tracks.get(currentPlaying), currentPlaying));
    }

    public Track getCurrentTrack(){
        return tracks.get(currentPlaying);
    }

    public void setCurrentPlaying(int currentPlaying) {
        for (int i = 0; i < tracks.size(); i++) { //Quitamos o ponemos background según se reproduce o no
            if (i == currentPlaying) {
                tracks.get(i).setPlaying(true);
            } else
                tracks.get(i).setPlaying(false);
        }
        this.currentPlaying = currentPlaying;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        //EventBus.getDefault().post(new SongFinishedEvent());
        playNextTrack();
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        //TODO
        // Al apretar muchas vece previous o next, tarda en reproducir (Async).

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer.start();
    }
}
