package com.demos.borjaruiz.webservices.responses.iTunes;

import com.demos.borjaruiz.model.Track;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TrackResponse {
    @SerializedName("resultCount")
    private int count;
    @SerializedName("results")
    private List<Track> trackList;

    public int getCount() {
        return count;
    }

    public List<Track> getTrackList() {
        return trackList;
    }
}
