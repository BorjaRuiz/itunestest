package com.demos.borjaruiz.utils.comparators;

import com.demos.borjaruiz.model.Track;

import java.util.Comparator;

public class TimeComparator implements Comparator<Track> {
    @Override
    public int compare(Track o1, Track o2) {
        return (int)(o1.getTrackTimeMillis()-o2.getTrackTimeMillis());
    }
}