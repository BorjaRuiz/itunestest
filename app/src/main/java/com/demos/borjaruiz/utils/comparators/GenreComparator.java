package com.demos.borjaruiz.utils.comparators;

import com.demos.borjaruiz.model.Track;

import java.util.Comparator;

public class GenreComparator implements Comparator<Track> {
    @Override
    public int compare(Track o1, Track o2) {
        return o1.getPrimaryGenreName().compareToIgnoreCase(o2.getPrimaryGenreName());
    }
}