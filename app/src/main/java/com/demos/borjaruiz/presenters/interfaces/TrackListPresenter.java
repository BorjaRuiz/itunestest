package com.demos.borjaruiz.presenters.interfaces;

import android.os.Bundle;
import android.view.View;

public interface TrackListPresenter {
    void findTracks(String searchText);

    void unRegisterEvents();

    void onItemClick(View v, int position);

    void orderBy(int orderName);

    Bundle getBundleToSave();

    void onRotateScreen();
}
