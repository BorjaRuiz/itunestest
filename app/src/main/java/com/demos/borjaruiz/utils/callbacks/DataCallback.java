package com.demos.borjaruiz.utils.callbacks;

public interface DataCallback { //CallBack para devolver resultados.
    public void onComplete(Object result);

    public void onNoData();

    public void onError(Error error);
}
