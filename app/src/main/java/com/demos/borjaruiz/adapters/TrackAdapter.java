package com.demos.borjaruiz.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.demos.borjaruiz.R;
import com.demos.borjaruiz.model.Track;
import com.demos.borjaruiz.ui.classes.CircleTransform;
import com.demos.borjaruiz.utils.callbacks.RecyclerViewClickListener;
import com.demos.borjaruiz.utils.helpers.StringHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrackAdapter extends RecyclerView.Adapter<TrackAdapter.Holder> {

    private final ArrayList<Track> items;
    private static RecyclerViewClickListener itemListener;
    private int layoutResId;

    public TrackAdapter(RecyclerViewClickListener itemListener, int layoutId) {
        items = new ArrayList<>();
        layoutResId = layoutId;
        TrackAdapter.itemListener = itemListener;
        setHasStableIds(true);
    }

    public void updateTracks(List<Track> list) {
        items.clear();
        items.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Track item = items.get(position);
        holder.setArtistName(item.getArtistName());
        holder.setTrackName(item.getTrackName());
        holder.setProfileImage(item.getArtworkUrl100());
        holder.setPrice(item.getTrackPrice());
        holder.setGenre(item.getPrimaryGenreName());
        holder.setTime(item.getTrackTimeMillis());
        holder.setPlaying(item.isPlaying());
    }


    @Override
    public long getItemId(int position) {
        return items.get(position).hashCode();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;
        @BindView(R.id.tv_track)
        TextView trackName;
        @BindView(R.id.tv_artist)
        TextView artistName;
        @BindView(R.id.iv_image)
        ImageView previewImage;
        @BindView(R.id.tv_genre)
        TextView genre;
        @BindView(R.id.tv_time)
        TextView time;
        @BindView(R.id.tv_price)
        TextView price;
        @BindView(R.id.card_view)
        CardView cardView;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this); //Activate row clicks.
            context = itemView.getContext();
        }

        void setPrice(double p) {
            price.setText(context.getResources().getString(R.string.placeholder_dollar, String.valueOf(p)));
        }

        void setTime(long t) {
            time.setText(StringHelper.getMinSecs(t));
        }

        void setGenre(String g) {
            genre.setText(g);
        }

        void setArtistName(String name) {
            artistName.setText(name);
        }

        void setTrackName(String name) {
            trackName.setText(name);
        }

        void setProfileImage(String img_url) {
            if (!TextUtils.isEmpty(img_url)) {
                Picasso.with(context).load(img_url).transform(new CircleTransform()).into(previewImage);
            } else {
                //placeholder image...
                Picasso.with(context).load(R.mipmap.ic_launcher).noFade().into(previewImage);
            }
        }

        void setPlaying(boolean playing) {
            if (playing) {
                cardView.setBackgroundResource(R.drawable.playing_background);
            } else {
                cardView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
            }
        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, getLayoutPosition()); //Returns click to activity
        }
    }

    public Track getItemAt(int position) {
        return items.get(position);
    }

}