package com.demos.borjaruiz.model.EventBus;

import com.demos.borjaruiz.model.Track;

public class SongPlayingEvent {
    public final Track currentTrack;
    public final int position;

    public SongPlayingEvent(Track track, int position) {
        currentTrack = track;
        this.position = position;
    }


}