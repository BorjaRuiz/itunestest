package com.demos.borjaruiz.interactors;

import com.demos.borjaruiz.utils.callbacks.DataCallback;

public interface DataInteractor {
    public void getData(String text, DataCallback callback);
}
