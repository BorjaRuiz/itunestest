package com.demos.borjaruiz.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.demos.borjaruiz.R;
import com.demos.borjaruiz.adapters.TrackAdapter;
import com.demos.borjaruiz.controllers.NavigationController;
import com.demos.borjaruiz.model.Track;
import com.demos.borjaruiz.presenters.implementations.TrackListPresenterImpl;
import com.demos.borjaruiz.presenters.interfaces.TrackListPresenter;
import com.demos.borjaruiz.ui.classes.SimpleDividerItemDecoration;
import com.demos.borjaruiz.ui.views.TrackListActivityView;
import com.demos.borjaruiz.utils.callbacks.RecyclerViewClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrackListActivity extends BaseAppCompatActivity implements RecyclerViewClickListener, TrackListActivityView {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SearchView searchView;
    TrackAdapter adapter;
    TrackListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_list);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        presenter = new TrackListPresenterImpl(this);
        adapter = new TrackAdapter(this, R.layout.row_track);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        recyclerView.setAdapter(adapter);

        if (savedInstanceState != null) {//Recuperamos las canciones al rotar la pantalla
            presenter.onRotateScreen();
        }
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        presenter.onItemClick(v, position);
    }

    @Override
    public void onNoResults(String text) {
        toast(getString(R.string.placeholder_no_tracks_result, text));
    }

    @Override
    public void refreshList(ArrayList<Track> refreshed_tracks) {
        adapter.updateTracks(refreshed_tracks);
    }

    @Override
    public void goDetailTrack(View view, Bundle bundle) {
        NavigationController.getInstance().goTrackDetailActivity(this, view, bundle);
    }

    @Override
    public void nothingToSort() {
        toast(getString(R.string.nothing_to_sort));
    }

    @Override
    public void showLoading() {
        loading();
    }

    @Override
    public void dismissDialog() {
        loaded();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putAll(presenter.getBundleToSave()); //Guardamos las canciones actuales al girar la pantalla
        super.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.findTracks(query); //busco canciones que coincidan

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {//not necessary in this case. (userful when filtering)
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //Seleccionado un modo de ordenación
        if (item.getItemId() != R.id.action_search) {
            presenter.orderBy(item.getItemId());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        presenter.unRegisterEvents(); //dejamos de escuchar eventos
        super.onDestroy();
    }
}
