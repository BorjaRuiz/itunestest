package com.demos.borjaruiz.utils;

public interface AppConstants {

    //WEBSERVICES
    public static final String API_BASE_URL = "https://itunes.apple.com";

    //TIPOGRAFIAS
    public static final String BOLD = "fonts/Roboto-Bold.ttf";
    public static final String THIN = "fonts/Roboto-Thin.ttf";
    public static final String LIGHT = "fonts/Roboto-Light.ttf";
    public static final String REGULAR = "fonts/Roboto-Regular.ttf";
    public static final String MEDIUM = "fonts/Roboto-Medium.ttf";
    public static final String ITALIC = "fonts/Roboto-Italic.ttf";

    public static final String STATE_ITEMS = "state_items";
    public static final String EXTRA_TRACK = "extra_track";
    public static final String EXTRA_POSITION = "extra_position";

    public static final String inputDateFormat = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String outputDateFormat = "dd/MM/yyyy";

    //ORDERING
    public static final int ORDER_PRICE = 0;
    public static final int ORDER_GENRE = 5;
    public static final int ORDER_TIME = 10;

}