package com.demos.borjaruiz.ui.views;

public interface TrackDetailActivityView {
    void loadDetails(String name, String artist, String image, String collection, String dur, String relase);

    void showPlayIcon();

    void showPauseIcon();

    void showBuyAlert(String title, String url);
}