package com.demos.borjaruiz.interactors;

import com.demos.borjaruiz.utils.callbacks.DataCallback;
import com.demos.borjaruiz.webservices.clients.ITunesClient;
import com.demos.borjaruiz.webservices.responses.iTunes.TrackResponse;
import com.demos.borjaruiz.webservices.services.ITunesService;

import retrofit2.Call;
import retrofit2.Response;

public class ITunesDataInteractorImpl implements DataInteractor {
    private ITunesClient client;

    public ITunesDataInteractorImpl() {
        client = ITunesService.createService(ITunesClient.class);
    }

    @Override
    public void getData(String text, final DataCallback callback) {
        Call<TrackResponse> call =
                client.getTracks(text);
        call.enqueue(new retrofit2.Callback<TrackResponse>() {
            @Override
            public void onResponse(Call<TrackResponse> call, Response<TrackResponse> response) {
                if (response.body().getTrackList().isEmpty())
                    callback.onNoData();
                else
                    callback.onComplete(response.body().getTrackList());
            }

            @Override
            public void onFailure(Call<TrackResponse> call, Throwable t) {
                callback.onError(new Error(t.getLocalizedMessage()));
            }
        });
    }
}
