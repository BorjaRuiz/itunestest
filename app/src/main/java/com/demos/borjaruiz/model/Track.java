package com.demos.borjaruiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Track implements Parcelable {
    @SerializedName("artistName")
    private String artistName;
    @SerializedName("collectionName")
    private String collectionName;
    @SerializedName("trackName")
    private String trackName;
    @SerializedName("trackId")
    private int trackId;
    @SerializedName("previewUrl")
    private String previewUrl;
    @SerializedName("artworkUrl100")
    private String artworkUrl100;
    @SerializedName("primaryGenreName")
    private String primaryGenreName;
    @SerializedName("releaseDate")
    private String releaseDate;
    @SerializedName("trackViewUrl")
    private String trackViewUrl;
    @SerializedName("trackPrice")
    private double trackPrice;
    @SerializedName("trackTimeMillis")
    private long trackTimeMillis;
    private boolean isPlaying;

    public String getArtistName() {
        return artistName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public int getTrackId() {
        return trackId;
    }

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    public double getTrackPrice() {
        return trackPrice;
    }

    public long getTrackTimeMillis() {
        return trackTimeMillis;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getTrackViewUrl() {
        return trackViewUrl;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.artistName);
        dest.writeString(this.collectionName);
        dest.writeString(this.trackName);
        dest.writeString(this.previewUrl);
        dest.writeString(this.artworkUrl100);
        dest.writeString(this.primaryGenreName);
        dest.writeString(this.releaseDate);
        dest.writeInt(this.trackId);
        dest.writeString(this.trackViewUrl);
        dest.writeDouble(this.trackPrice);
        dest.writeLong(this.trackTimeMillis);
        dest.writeByte(this.isPlaying ? (byte) 1 : (byte) 0);
    }

    public Track() {
    }

    protected Track(Parcel in) {
        this.artistName = in.readString();
        this.collectionName = in.readString();
        this.trackName = in.readString();
        this.previewUrl = in.readString();
        this.artworkUrl100 = in.readString();
        this.primaryGenreName = in.readString();
        this.releaseDate = in.readString();
        this.trackId = in.readInt();
        this.trackViewUrl = in.readString();
        this.trackPrice = in.readDouble();
        this.trackTimeMillis = in.readLong();
        this.isPlaying = in.readByte() != 0;
    }

    public static final Creator<Track> CREATOR = new Creator<Track>() {
        @Override
        public Track createFromParcel(Parcel source) {
            return new Track(source);
        }

        @Override
        public Track[] newArray(int size) {
            return new Track[size];
        }
    };
}
