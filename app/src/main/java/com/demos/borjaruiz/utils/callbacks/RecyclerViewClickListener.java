package com.demos.borjaruiz.utils.callbacks;

import android.view.View;

public interface RecyclerViewClickListener
{
    public void recyclerViewListClicked(View v, int position);
}