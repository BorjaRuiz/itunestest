package com.demos.borjaruiz;

import android.app.Application;

import com.demos.borjaruiz.utils.AppConstants;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppApplication extends Application {
    private static AppApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        //Fuente custom por defecto.
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(AppConstants.REGULAR)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public static synchronized AppApplication getInstance() {
        return mInstance;
    }
}
