package com.demos.borjaruiz.presenters.implementations;

import com.demos.borjaruiz.controllers.MediaPlayerController;
import com.demos.borjaruiz.model.EventBus.SongFinishedEvent;
import com.demos.borjaruiz.model.EventBus.SongPlayingEvent;
import com.demos.borjaruiz.model.Track;
import com.demos.borjaruiz.presenters.interfaces.TrackDetailsPresenter;
import com.demos.borjaruiz.ui.views.TrackDetailActivityView;
import com.demos.borjaruiz.utils.helpers.StringHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class TrackDetailsPresenterImpl implements TrackDetailsPresenter {
    private TrackDetailActivityView view;

    public TrackDetailsPresenterImpl(TrackDetailActivityView view) {
        this.view = view;
        //Empiezo a reproducir la canción seleccionada
        registerEvents();
        getMediaPlayer().play();
    }

    @Override
    public void playOrPause() {
        if (getMediaPlayer().isPlaying() || getMediaPlayer().isPaused()) {
            getMediaPlayer().switchState();
            if (getMediaPlayer().isPaused()) {
                view.showPlayIcon();
            } else
                view.showPauseIcon();
        } else {
            getMediaPlayer().play();
            view.showPauseIcon();
        }
    }

    private void registerEvents() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unRegisterEvents() {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onSongPlayingEvent(SongPlayingEvent event) { //Salta cada vez que empieza a repdroducirse una canción
        view.showPauseIcon();
        updateTrackDetails();
    }

    @Subscribe
    public void onSongFinishedEvent(SongFinishedEvent event) {
        //Podríamos usar este evento para hacer algo al acabar de sonar una canción
    }

    @Override
    public void nextTrack() {
        getMediaPlayer().playNextTrack();
        updateTrackDetails();
    }

    private void updateTrackDetails() {
        view.loadDetails(currentTrack().getTrackName(), currentTrack().getArtistName(), currentTrack().getArtworkUrl100(), currentTrack().getCollectionName(), StringHelper.getMinSecs(currentTrack().getTrackTimeMillis()), StringHelper.formateDateFromstring(currentTrack().getReleaseDate()));
    }

    @Override
    public void previousTrack() {
        getMediaPlayer().playPreviousTrack();
        updateTrackDetails();
    }

    @Override
    public void buyClicked() {
        view.showBuyAlert(getMediaPlayer().getCurrentTrack().getTrackName(), getMediaPlayer().getCurrentTrack().getTrackViewUrl());
    }

    private MediaPlayerController getMediaPlayer() {
        return MediaPlayerController.getInstance();
    }

    private Track currentTrack() {
        return getMediaPlayer().getCurrentTrack();
    }
}
