package com.demos.borjaruiz.presenters.interfaces;

public interface TrackDetailsPresenter {
    void playOrPause();

    void unRegisterEvents();

    void nextTrack();

    void previousTrack();

    void buyClicked();
}
